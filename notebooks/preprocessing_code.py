df_simple_model = spark.sql("SELECT user.user_int_id,\
                                    bus.business_int_id,\
                                    base.stars\
                                FROM base_recc_df as base\
                                    LEFT JOIN bus_id_index as bus\
                                        ON base.business_id=bus.business_id\
                                    LEFT JOIN user_id_index as user\
                                        ON base.user_id=user.user_id")
