from pyspark.sql import SparkSession

spark = SparkSession \
 .builder \
 .appName("NLP Modeling") \
 .config("spark.some.config.option", "some-value") \
 .getOrCreate()

# Step 1 : import relevant files
df = spark.read.json("../../yelp/dataset/review.json")
# using these to for index value purposes
user_df = spark.read.json("../../yelp/dataset/user.json")
bus_df = spark.read.json("../../yelp/dataset/business.json")

# Step 2-0, basic model needs
base_recc_df = df.select('user_id','business_id','stars')
# For NLP processing
text = df.select('text')

# Create integers of values using indexes 
from pyspark.sql.functions import monotonically_increasing_id 

bus_id_index = bus_df.select("business_id").withColumn("business_int_id", monotonically_increasing_id())
user_id_index = user_df.select("user_id").withColumn("user_int_id", monotonically_increasing_id())

bus_id_index.createTempView('bus_id_index')
user_id_index.createTempView('user_id_index')
base_recc_df.createTempView('base_recc_df')


df_simple_model = spark.sql("SELECT user.user_int_id,\
                                      bus.business_int_id,\
                                      base.stars\
                                FROM base_recc_df as base\
                                    LEFT JOIN bus_id_index as bus\
                                        ON base.business_id=bus.business_id\
                                    LEFT JOIN user_id_index as user\
                                        ON base.user_id=user.user_id").show(50,False)