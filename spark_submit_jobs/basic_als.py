from pyspark.sql import SQLContext
from pyspark.sql.types import IntegerType
from pyspark.sql.functions import monotonically_increasing_id
from pyspark.mllib.recommendation import ALS, MatrixFactorizationModel, Rating

#initialize sqlcontext
spark = SQLContext(sc)

rawdata = sc.textFile("/user/pkouvaris/pk_data/wfcc_201712_reccdata.csv")
csv_tupled = rawdata.map(lambda l: l.split(","))
rows = csv_tupled.map(lambda l: (l[0],l[1],l[2]))

ratings_df = spark.createDataFrame(rows)

# create integer id version
new_membid_df = ratings_df.select('_1').distinct()\
                .withColumn('int_membid', monotonically_increasing_id().cast(IntegerType()))

ratings_df.registerTempTable('ratings_df')
new_membid_df.registerTempTable('new_membid_df')

pre_proc_df = spark.sql("select b.int_membid, a._2, a._3\
                         from ratings_df a left join new_membid_df\
                         b on a._1=b._1\
                         where a._2 != ''")

pre_proc_df.registerTempTable('pre_prtoc_df')

ratings = pre_proc_df.map(lambda l: Rating( l.int_membid, int(l._2), int(float(l._3)) ) )

model = ALS.train(ratings, 10, 10)

# Evaluate the model on training data
testdata = ratings.map(lambda p: (p[0], p[1]))

predictions = model.predictAll(testdata)

pred_df = spark.createDataFrame(predictions)
pred_df.registerTempTable('pred_df')

final = spark.sql("select a._1 as membccid,\
                          b._2 as category,\
                          b._3 as real_spend,\
                          c.rating as pred_spend\
                    from new_membid_df a\
                        left join ratings_df b\
                            on a._1=b._1\
                        left join pred_df c\
                            on a.int_membid=c.user and b._2=c.product")

final = spark.createDataFrame(final)

# predictions = model.predictAll(testdata).map(lambda r: ((r[0], r[1]), r[2]))

# ratesAndPreds = ratings.map(lambda r: ((r[0], r[1]), r[2])).join(predictions)

# MSE = ratesAndPreds.map(lambda r: (r[1][0] - r[1][1])**2).mean()

# print("Mean Squared Error = " + str(MSE))


