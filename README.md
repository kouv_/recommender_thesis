# Restaraunt Recommendations At Scale
### A Machine Learning Review of Effective NLP Recommendation Systems

## The Team
- Albert Asuncion - Research Lead + Product Manager
- Ekaterina Pirogova - NLP Sentiment Engineering
- Hari Narayan Sanadhya - Recommender System Engineering
- Peter Kouvaris - NLP Preprocessing 

## The Approach
Our team is testing out pipelines that allow for NLP data to be consumed in recommender systems using Yelp's open data set as an initial universe. Our apporach is straightforward:
1. Reweight and as a result normalize user ratings based on each users word usage during a review's text field. 
2. Engineer features from the text about each users specific sentiment towards that user's food experience.
3. Build a recommender system measuring the variations in the above pipelines with quantiative and qualitative measures.
